Design Happy are a hands on, independent branding & packaging design agency. We deliver unique and impactful brand identities, commercially effective packaging and beautiful branding across the marketing mix. ||

Address: The Cowshed, 26 Bloomfield Road, Kingston Upon Thames KT1 2SE, UK ||
Phone: +44 20 3816 0245
